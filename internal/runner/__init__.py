from configparser import ConfigParser

from internal.infrastructure.uwsgi import Server
from internal.domain.resource_service import ResourceServer
from internal.infrastructure.pg_storage import PgStorage
from internal.infrastructure.http_api_server import HttpAPI

def get_http_api_app():
    config = read_config()

    storage = PgStorage(
        host=config['db_host'],
        port=config['db_port'],
        dbname=config['db_name'],
        user=config['db_user'],
        password=config['db_pass']
    )
    resource_server = ResourceServer(storage)
    api = HttpAPI(resource_server)
    app = Server(config['api_prefix'])

    @app.route('GET', '/resource_types')
    def list_types(request, response):
        api.list_resource_types(request, response)

    @app.route('POST', '/resource_types')
    def create_type(request, response):
        api.create_resource_type(request, response)

    @app.route('GET', '/resource_types/{id}')
    def get_type(request, response, id):
        api.get_resource_type(id, request, response)

    @app.route('PUT', '/resource_types/{id}')
    def update_type(request, response, id):
        api.update_resource_type(id, request, response)

    @app.route('DELETE', '/resource_types/{id}')
    def delete_resource(request, response, id):
        api.delete_resource_type(id, request, response)

    @app.route('DELETE', '/resource_types')
    def delete_types(request, response):
        api.delete_resource_types(request, response)

    @app.route('GET', '/resources')
    def list_resources(request, response):
        api.list_resources(request, response)

    @app.route('POST', '/resources')
    def create_resources(request, response):
        api.create_resource(request, response)

    @app.route('GET', '/resources/{id}')
    def get_resource(request, response, id):
        api.get_resource(id, request, response)

    @app.route('PUT', '/resources/{id}')
    def update_type(request, response, id):
        api.update_resource(id, request, response)

    @app.route('DELETE', '/resources/{id}')
    def delete_resource(request, response, id):
        api.delete_resource(id, request, response)

    @app.route('DELETE', '/resources')
    def delete_resources(request, response):
        api.delete_resources(request, response)

    return app


def read_config() -> dict:
    config = ConfigParser()
    config.read('config.ini')

    res = {}

    if 'server' not in config.sections():
        raise KeyError("config section server not found")

    res['api_prefix'] = config['server'].get('api_prefix')

    if 'database' not in config.sections():
        raise KeyError("database section server not found")

    db_type = config['database'].get('type', '')
    if db_type != 'pgsql':
        raise NotImplementedError('database type ' + db_type + ' is not supported')

    res['db_host'] = config['database'].get('host', '')
    res['db_port'] = str(config['database'].get('port', ''))
    res['db_name'] = config['database'].get('database', '')
    res['db_user'] = config['database'].get('user', '')
    res['db_pass'] = config['database'].get('password', '')

    return res
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Optional, Union
from internal.domain.models.resource_type import NewResourceTypeDTO, UpdateResourceTypeDTO, ResourceType
from internal.domain.models.resource import NewResourceDTO, UpdateResourceDTO, Resource
from internal.utils.result import Result


@dataclass
class DBError(Exception):
    description: str = ""


class NotFound(Exception):
    pass


Error = Union[NotFound, DBError]


class Storage(ABC):

    @abstractmethod
    def create_type(self, item: NewResourceTypeDTO) -> Result[ResourceType, Error]:
        #
        pass

    @abstractmethod
    def read_types(self, ids: Optional[list[int]] = None, names: Optional[list[str]] = None) -> Result[list[ResourceType], Error]:
        #
        pass

    @abstractmethod
    def update_type(self, item: UpdateResourceTypeDTO) -> Result[ResourceType, Error]:
        #
        pass

    @abstractmethod
    def delete_types(self, ids: list[int]) -> Result[list[ResourceType], Error]:
        #
        pass

    @abstractmethod
    def create_resource(self, item: NewResourceDTO) -> Result[list[Resource], Error]:
        pass

    @abstractmethod
    def read_resources(self, ids: Optional[list[int]] = None, types: Optional[list[str]] = None) -> Result[list[Resource], Error]:
        #
        pass

    @abstractmethod
    def update_resource(self, item: UpdateResourceTypeDTO) -> Result[Resource, Error]:
        #
        pass

    @abstractmethod
    def delete_resources(self, ids: list[int]) -> Result[list[Resource], Error]:
        pass
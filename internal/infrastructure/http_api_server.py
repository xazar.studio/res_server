import json

from internal.domain.resource_service import (
    Resource,
    NewResourceDTO,
    UpdateResourceDTO,
    ResourceType,
    NewResourceTypeDTO,
    UpdateResourceTypeDTO,
    ResourceServer,
    ValidationError,
    NotFound)

from internal.utils.result import *
from http import HTTPStatus
from internal.infrastructure.uwsgi import Request, Response
from typing import Optional
from dataclasses import dataclass

from internal.utils.validators import check_object, check_integer, check_string


@dataclass
class QsError(Exception):
    description: str = ''

@dataclass
class DataError(Exception):
    description: str = ''


class HttpAPI:
    __srv: ResourceServer

    def __init__(self, res_srv: ResourceServer):
        self.__srv = res_srv

    def list_resource_types(self, req: Request, resp: Response) -> Response:
        result = (
            parse_ids(req.qs)
            .and_then(self.__srv.get_resource_types)
            .map(lambda items: list(map(encode_type, items)))
        )
        return handle_result(result, resp)

    def create_resource_type(self, req: Request, resp: Response) -> Response:
        try:
            body = check_object(req.body).map_err(lambda e: DataError(e)).unwrap()
            name = check_string(body.get('name', '')).map_err(lambda e: DataError(f"'name' {e}"))
            max_speed = check_integer(body.get('max_speed', '')).map_err(lambda e: DataError(f"'max_speed' {e}"))
            result = (
                self.__srv.create_resource_type(NewResourceTypeDTO(name.unwrap(), max_speed.unwrap()))
                .map(encode_type)
            )
        except DataError as err:
            result = Err(err)
        return handle_result(result, resp)

    def get_resource_type(self, id: str, _req: Request, resp: Response) -> Response:
        result = (
            check_integer(id)
            .map_err(lambda _: NotFound())
            .and_then(lambda i: self.__srv.get_resource_types([id]))
            .and_then(lambda items: Err(NotFound()) if items == [] else Ok(items[0]))
            .map(encode_type)
        )
        return handle_result(result, resp)

    def update_resource_type(self, id: str, req: Request, resp: Response) -> Response:
        try:
            type_id = check_integer(id).map_err(lambda _: NotFound())
            body = check_object(req.body).map_err(lambda e: DataError(e)).unwrap()
            name = check_string(body.get('name', '')).map_err(lambda e: DataError(f"'name' {e}"))
            max_speed = check_integer(body.get('max_speed', '')).map_err(lambda e: DataError(f"'max_speed' {e}"))
            update_req = UpdateResourceTypeDTO(
                type_id.unwrap(),
                name.unwrap(),
                max_speed.unwrap()
            )
            result = self.__srv.update_resource_type(update_req).map(encode_type)
        except NotFound as err:
            result = Err(err)
        except DataError as err:
            result = Err(err)
        return handle_result(result, resp)

    def delete_resource_type(self, id: int, _req: Request, resp: Response) -> Response:
        result = (
            self.__srv.delete_resource_types(ids=[id])
            .and_then(lambda items: Err(NotFound()) if items == [] else Ok(items[0]))
            .map(encode_type)
        )
        return handle_result(result, resp)

    def delete_resource_types(self, req: Request, resp: Response) -> Response:
        result = (
            parse_ids(req.qs)
            .and_then(self.__srv.delete_resource_types)
            .map(lambda items: list(map(encode_type, items)))
        )
        return handle_result(result, resp)

    def create_resource(self, req: Request, resp: Response) -> Response:
        try:
            body = check_object(req.body).map_err(lambda e: DataError(e)).unwrap()
            name = check_string(body.get('name', '')).map_err(lambda e: DataError(f"'name' {e}")).unwrap()
            type = check_string(body.get('type', '')).map_err(lambda e: DataError(f"'type' {e}")).unwrap()
            speed = check_integer(body.get('speed', '')).map_err(lambda e: DataError(f"'speed' {e}")).unwrap()
            result = self.__srv.create_resource(NewResourceDTO(name, speed, type)).map(encode_resource)
        except DataError as err:
            result = Err(err)

        return handle_result(result, resp)

    def update_resource(self, id: str, req: Request, resp: Response) -> Response:
        try:
            resource_id = check_integer(id).map_err(lambda _: NotFound()).unwrap()
            body = check_object(req.body).map_err(lambda e: DataError(e)).unwrap()
            name = check_string(body.get('name', '')).map_err(lambda e: DataError(f"'name' {e}")).unwrap()
            type = check_string(body.get('type', '')).map_err(lambda e: DataError(f"'type' {e}")).unwrap()
            speed = check_integer(body.get('speed', '')).map_err(lambda e: DataError(f"'speed' {e}")).unwrap()
            result = self.__srv.update_resource(UpdateResourceDTO(resource_id, name, speed, type)).map(encode_resource)
        except NotFound as err:
            result = Err(err)
        except DataError as err:
            result = Err(err)
        return handle_result(result, resp)

    def list_resources(self, req: Request, resp: Response) -> Response:
        try:
            qs = req.qs
            ids = parse_ids(qs).unwrap()
            types = parse_types(qs).unwrap()
            result = (
                self.__srv.get_resources(ids, types)
                .map(lambda items: list(map(encode_resource, items)))
            )
        except QsError as err:
            result = Err(err)
        return handle_result(result, resp)

    def get_resource(self, id: str, _req: Request, resp: Response) -> Response:
        result = (
            check_integer(id)
            .map_err(lambda _: NotFound())
            .and_then(lambda i: self.__srv.get_resources(ids=[id]))
            .and_then(lambda items: Err(NotFound()) if items == [] else Ok(items[0]))
            .map(encode_resource)
        )
        return handle_result(result, resp)

    def delete_resource(self, id: int, _req: Request, resp: Response) -> Response:
        result = (
            self.__srv.delete_resources(ids=[id])
            .and_then(lambda items: Err(NotFound()) if items == [] else Ok(items[0]))
            .map(encode_resource)
        )
        return handle_result(result, resp)

    def delete_resources(self, req: Request, resp: Response) -> Response:
        result = (
            parse_ids(req.qs)
            .and_then(self.__srv.delete_resources)
            .map(lambda items: list(map(encode_resource, items)))
        )
        return handle_result(result, resp)

def parse_ids(qs: dict) -> Result[Optional[list[int]], QsError]:
    match qs:
        case {'id': list(ids)}:
            try:
                return Ok([int(id) for id in ids])
            except ValueError:
                return Err(QsError('id param must be valid integer'))
        case _:
            return Ok(None)

def parse_types(qs: dict) -> Result[Optional[list[str]], QsError]:
    match qs:
        case {'type': list(types)}:
            try:
                return Ok([str(t) for t in types])
            except ValueError:
                return Err(QsError('type param must be valid string'))
        case _:
            return Ok(None)


def handle_result(result, resp: Response) -> Response:
    match result:
        case Ok(result):
            resp.set_body(json.dumps(result))
            return resp
        case Err(NotFound()):
            return not_found(resp, json.dumps("Not found"))
        case Err(QsError(err)):
            return bad_qs(resp, json.dumps(f"Bad qs: {err}"))
        case Err(DataError(err)):
            return bad_data(resp, json.dumps(f"Bad data: {err}"))
        case Err(ValidationError(err)):
            return bad_data(resp, json.dumps(f"Bad data: {err}"))
        case other:
            return internal_error(resp, json.dumps(f"Internal error {other}"))

def internal_error(resp: Response, body: Optional[str] = None) -> Response:
    resp.set_status(HTTPStatus.INTERNAL_SERVER_ERROR)
    resp.set_body(body)
    return resp


def not_found(resp: Response, body: Optional[str] = None) -> Response:
    resp.set_status(HTTPStatus.NOT_FOUND)
    resp.set_body(body)
    return resp


def bad_qs(resp: Response, body: Optional[str] = None) -> Response:
    resp.set_status(HTTPStatus.BAD_REQUEST)
    resp.set_body(body)
    return resp


def bad_data(resp: Response, body: Optional[str] = None) -> Response:
    resp.set_status(HTTPStatus.BAD_REQUEST)
    resp.set_body(body)
    return resp


def encode_type(type: ResourceType) -> dict:
    return {
        'id': type.id,
        'name': type.name,
        'max_speed': type.max_speed
    }


def encode_resource(resource: Resource) -> dict:
    return {
        'id': resource.id,
        'name': resource.name,
        'type': resource.type.name,
        'speed': resource.speed,
        'overSpeed': resource.over_speed
    }
import psycopg2
from psycopg2 import OperationalError, errors
import sys

from typing import Optional
from internal.ports.storage import Storage, DBError, Error, NotFound
from internal.domain.models.resource_type import ResourceType, NewResourceTypeDTO, UpdateResourceTypeDTO
from internal.domain.models.resource import Resource, NewResourceDTO, UpdateResourceDTO
from internal.utils.result import *

class PgStorage(Storage):
    # __conn = None
    __host: str = ''
    __port: int = 5432
    __dbname: str = ''
    __user: str = ''
    __password: str = ''

    def __init__(self, host: str, port: int, dbname: str, user: str, password: str) -> None:
        self.__host = host
        self.__port = port
        self.__dbname = dbname
        self.__user = user
        self.__password = password

    def __connection(self):
        return psycopg2.connect(
            host=self.__host,
            port=self.__port,
            dbname=self.__dbname,
            user=self.__user,
            password=self.__password
        )
    def __execute(self, sql, args):
        try:
            with self.__connection() as conn, conn.cursor() as curr:
                curr.execute(sql, args)
                return Ok(curr.fetchall())
        except Exception as err:
            return Err(DBError(str(err)))


    def create_type(self, item: NewResourceTypeDTO) -> Result[ResourceType, Error]:

        query = """
            INSERT INTO resource_types (name, max_speed)
            VALUES (%s, %s)
            RETURNING id, name, max_speed
        """

        match self.__execute(query, (item.name, item.max_speed)):
            case Ok([(id, name, max_speed)]):
                return Ok(ResourceType(id, name, max_speed))
            case Err(_) as err:
                return err

    def read_types(self, ids: Optional[list[int]] = None, names: Optional[list[str]] = None) -> Result[list[ResourceType], Error]:
        args = []

        ids_conditions = ''
        if ids:
            ids_conditions = '(' + ' OR '.join(str("t.id = %s") for _ in ids) + ') '
            args += ids

        names_conditions = ''
        if names:
            names_conditions = ' (' + ' OR '.join(str("t.name = %s") for _ in names) + ') '
            args += names

        conditions = ' AND '.join([c for c in [ids_conditions, names_conditions] if c])

        where = ''
        if conditions:
            where = ' WHERE ' + conditions

        query = f"""
            SELECT id, name, max_speed FROM resource_types t
            {where}
            ORDER BY t.id
        """

        match self.__execute(query, tuple(args)):
            case Ok(data):
                return Ok([ResourceType(id, name, max_speed) for id, name, max_speed in data])
            case Err(_) as err:
                return err

    def update_type(self, item: UpdateResourceTypeDTO) -> Result[ResourceType, Error]:

        query = """
            UPDATE resource_types SET name=%s, max_speed=%s WHERE id=%s
            RETURNING id, name, max_speed
        """

        match self.__execute(query, (item.name, item.max_speed, item.id)):
            case Ok(data) if len(data) == 0:
                return Err(NotFound())
            case Ok([(id, name, max_speed)]):
                return Ok(ResourceType(id, name, max_speed))
            case Err(_) as err:
                return err

    def delete_types(self, ids: list[int]) -> Result[list[ResourceType], Error]:
        if not ids:
            return Err(ValueError("At least one 'id' qs param must be specified"))

        query = """
            DELETE FROM resource_types
        """
        query += ' WHERE ' + " OR ".join(str("id = %s") for _ in ids)
        query += ' returning id, name, max_speed'

        match self.__execute(query, tuple(ids)):
            case Ok(data):
                return Ok([ResourceType(id, name, max_speed) for id, name, max_speed in data])
            case Err(_) as err:
                return err

    def read_resources(self, ids: Optional[list[int]] = None, types: Optional[list[str]] = None) -> Result[list[Resource], Error]:
        args = []

        ids_conditions = ''
        if ids:
            ids_conditions = '(' + ' OR '.join(str("r.id = %s") for _ in ids) + ') '
            args += ids

        types_conditions = ''
        if types:
            types_conditions = ' (' + ' OR '.join(str("t.name = %s") for _ in types) + ') '
            args += types

        conditions = ' AND '.join([c for c in [ids_conditions, types_conditions] if c])

        where = ''
        if conditions:
            where = ' WHERE ' + conditions

        query = f"""
            SELECT r.id, r.name, r.cur_speed, t.id, t.name, t.max_speed FROM resources r
            JOIN resource_types t on r.resource_type = t.id
            {where}
            ORDER BY r.id
        """

        match self.__execute(query, tuple(args)):
            case Ok(data):
                resources = [
                    Resource(id, name, ResourceType(t_id, t_name, t_max_speed), speed)
                    for id, name, speed, t_id, t_name, t_max_speed in data
                ]
                return Ok(resources)
            case Err(_) as err:
                return err

    def create_resource(self, req: NewResourceDTO) -> Result[Resource, Error]:
        query = """
            insert into resources (name, cur_speed, resource_type) 
            values(%s, %s, %s)
            returning id, name, cur_speed
        """
        match self.read_types(names=[req.type]):
            case Ok([type]):
                match self.__execute(query, (req.name, req.speed, type.id)):
                    case Ok([(id, name, speed)]):
                        return Ok(Resource(id, name, type, speed))
                    case Err(_) as err:
                        return err
            case Ok([]):
                return Err(DBError('type not found'))
            case other:
                return other

    def update_resource(self, req: UpdateResourceDTO) -> Result[Resource, Error]:
        query = """
            UPDATE resources SET name=%s, cur_speed=%s, resource_type=%s WHERE id=%s
            RETURNING id, name, cur_speed
        """
        match self.read_types(names=[req.type]):
            case Ok([type]):
                match self.__execute(query, (req.name, req.speed, type.id, req.id)):
                    case Ok(data) if len(data) == 0:
                        return Err(NotFound())
                    case Ok([(id, name, speed)]):
                        return Ok(Resource(id, name, type, speed))
                    case Err(_) as err:
                        return err
            case Ok([]):
                return Err(DBError('type not found'))
            case other:
                return other


    def delete_resources(self, ids: list[int]) -> Result[list[Resource], Error]:
        if not ids:
            return Err(ValueError("At least one 'id' qs param must be specified"))

        condition = " OR ".join(str("r.id = %s") for _ in ids)
        query = f"""
            DELETE FROM resources r
            USING resource_types t
            WHERE (r.resource_type = t.id) AND {condition}
            returning r.id, r.name, r.cur_speed, t.id, t.name, t.max_speed
        """

        match self.__execute(query, tuple(ids)):
            case Ok(data):
                resources = [
                    Resource(id, name, ResourceType(t_id, t_name, t_max_speed), speed)
                    for id, name, speed, t_id, t_name, t_max_speed in data
                ]
                return Ok(resources)
            case Err(_) as err:
                return err

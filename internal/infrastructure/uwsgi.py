import re
from http import HTTPStatus
from urllib.parse import parse_qs
from typing import Optional, Iterator, Callable
import json


class Request:
    method: str = ''
    path: str = ''
    qs: dict = {}
    body: Optional[dict]

    def __init__(self, env):
        self.method = env['REQUEST_METHOD']
        self.path = env['PATH_INFO']
        self.qs = parse_qs(env['QUERY_STRING'])
        self.body = maybe_read_body(env)


def maybe_read_body(env):
    if env['REQUEST_METHOD'] not in ['POST', 'PUT']:
        return None
    if env.get('CONTENT_TYPE', '') != 'application/json':
        return None
    content_length = 0
    try:
        content_length = int(env.get('CONTENT_LENGTH', 0))
    except (ValueError):
        content_length = 0
    if content_length == 0:
        return None
    request_body = env['wsgi.input'].read(content_length)
    try:
        return json.loads(request_body)
    except:
        return None


class Response:
    __status: HTTPStatus = HTTPStatus.OK
    __headers: list[(str, str)] = []
    __body: Optional[str] = None

    def __init__(self):
        self.__headers = [
            ('Content-Type', 'application/json')
        ]
        self.__body = None

    def __call__(self, start_response) -> Iterator[bytes]:
        code_str = str(self.__status.value)

        if self.__body is None:
            start_response(code_str, self.__headers)
            return iter([])
        else:
            self.add_header('Content-Length', str(len(self.__body)))
            start_response(code_str, self.__headers)
            return iter([bytes(self.__body, 'utf-8')])

    def set_status(self, status: HTTPStatus):
        self.__status = status

    def set_body(self, body: Optional[str]):
        self.__body = body

    def add_header(self, key: str, value: str):
        self.__headers += [(key, value)]


Handler = Callable[[Request, Response], Response]

class Server:
    __routes: dict[re.Pattern, dict[str, Handler]] = {}
    __api_prefix: str = ''

    def __init__(self, api_prefix: str = ''):
        self.__api_prefix = api_prefix

    def __call__(self, env, start_response):
        request = Request(env)
        response = self.handle_request(request)
        return response(start_response)

    def route(self, method: str, path: str):
        def wrapper(handler):
            self.add_handler(method, path, handler)
            return handler

        return wrapper

    def add_handler(self, method: str, path: str, handler):
        regex = re.compile(path_to_regex(self.__api_prefix + path))
        self.__routes.setdefault(regex, {})[method] = handler
        return self

    def handle_request(self, request):
        response = Response()
        handler, kwargs = self.find_handler(request.method, request.path)
        handler(request, response, **kwargs)
        return response

    def find_handler(self, method: str, path: str) -> (Handler, dict):
        for regex, methods in self.__routes.items():
            match = regex.match(path)
            if match:
                handler = methods.get(method)
                if handler:
                    return handler, match.groupdict()
                else:
                    return UnknownMethodHandler(list(methods.keys())), {}
            else:
                continue
        return NotFoundHandler(), {}

def path_to_regex(path: str):
    patterns = []
    for i in path.strip('/').split("/"):
        if i == '*':
            patterns.append(".*")
        if i[0] == "{" and i[-1] == "}":
            tag = i[1:-1]
            patterns.append(f"(?P<{tag}>[^/]*)")
        else:
            patterns.append(i)
    return "^/" + "/".join(patterns) + "/?$"

class UnknownMethodHandler:
    __known_methods: list[str] = []

    def __init__(self, methods: list[str]):
        self.__known_methods = methods

    def __call__(self, req: Request, resp: Response) -> Response:
        resp.set_status(HTTPStatus.METHOD_NOT_ALLOWED)
        resp.add_header('Allow', ', '.join(self.__known_methods))
        return resp


class NotFoundHandler:
    __body: Optional[str] = None
    def __init__(self, body: Optional[str] = None):
        self.__body = body
    def __call__(self, req: Request, resp: Response) -> Response:
        resp.set_status(HTTPStatus.NOT_FOUND)
        resp.set_body(self.__body)
        return resp

from typing import Any

from internal.utils.result import Result, Err, Ok

def check_object(value: Any) -> Result[dict, str]:
    if not isinstance(value, dict):
        return Err("must be valid object")
    return Ok(value)


def check_integer(value: Any) -> Result[int, str]:
    try:
        return Ok(int(value))
    except ValueError:
        return Err("must be valid integer")

def check_string(value: Any) -> Result[str, str]:
    if not isinstance(value, str):
        return Err("must be valid string")
    name = value.strip()
    if name == '' or len(name) > 255:
        return Err("must be non empty string max 255 chars")
    return Ok(name)

from __future__ import annotations

from typing import Any, Callable, Generic, TypeAlias, TypeVar, Union

T = TypeVar("T")
E = TypeVar("E", bound=BaseException)
U = TypeVar("U")
F = TypeVar("F")

class Ok(Generic[T]):
    _value: T
    __match_args__ = ("_value",)

    def __init__(self, value: T):
        self._value = value

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, Ok):
            return self._value == other._value  # type: ignore
        return False

    def __repr__(self) -> str:
        return f"Ok({repr(self._value)})"

    def map(self, op: Callable[[T], U]) -> Ok[U]:
        return Ok(op(self._value))

    def map_err(self, _op: Callable[[T], U]) -> Ok[T]:
        return self

    def and_then(self, op: Callable[[T], Result[U, E]]) -> Result[U, E]:
        return op(self._value)

    def or_else(self, _op: object) -> Ok[T]:
        return self

    def unwrap(self) -> T:
        return self._value


class Err(Generic[E]):
    _err: E
    __match_args__ = ("_err",)

    def __init__(self, err: E):
        self._err = err

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, Err):
            return self._err == other._err  # type: ignore
        return False

    def __repr__(self) -> str:
        return f"Err({repr(self._err)})"

    def map(self, _op: Callable[[T], U]) -> Err[E]:
        return self

    def map_err(self, op: Callable[[E], F]) -> Err[F]:
        return Err(op(self._err))

    def and_then(self, _op: object) -> Err[E]:
        return self

    def or_else(self, op: Callable[[E], Result[T, F]]) -> Result[T, F]:
        return op(self._err)

    def unwrap(self):
        raise self._err


Result: TypeAlias = Union[Ok[T], Err[E]]

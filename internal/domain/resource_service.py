from internal.utils.result import *
from typing import Optional
from internal.ports.storage import Storage, NotFound as DBNotFound, DBError

from internal.domain.models.resource_type import *
from internal.domain.models.resource import *


@dataclass
class ValidationError(Exception):
    description: str = ""


@dataclass
class InternalError(Exception):
    description: str = ""

@dataclass
class NotFound(Exception):
    pass


Error = Union[ValidationError, InternalError, NotFound]


class ResourceServer:
    __storage: Storage

    def __init__(self, storage: Storage):
        self.__storage = storage

    def get_resource_types(self, ids: Optional[list[ResourceType]] = None) -> Result[list[ResourceType], Error]:
        return self.__storage.read_types(ids)

    def create_resource_type(self, req: NewResourceTypeDTO) -> Result[ResourceType, Error]:
        if req.name.strip() == "":
            return Err(ValidationError("'name' can't be empty"))
        if req.max_speed <= 0:
            return Err(ValidationError("'max_speed' must be greater than zero"))

        match self.__storage.create_type(req):
            case Ok(resource_type):
                return Ok(resource_type)
            case Err(err):
                return Err(InternalError(f"Internal error {err}"))

    def update_resource_type(self, req: UpdateResourceTypeDTO) -> Result[ResourceType, Error]:
        if req.name.strip() == "":
            return Err(ValidationError("'name' can't be empty"))
        if req.max_speed <= 0:
            return Err(ValidationError("'max_speed' must be greater than zero"))

        match self.__storage.update_type(req):
            case Ok(resource_type):
                return Ok(resource_type)
            case Err(DBNotFound()):
                return Err(NotFound())
            case Err(err):
                return Err(InternalError(f"Internal error {err}"))

    def delete_resource_types(self, ids: list[int]) -> Result[list[ResourceType], Error]:
        match self.__storage.delete_types(ids):
            case Ok(removed):
                return Ok(removed)
            case Err(err):
                return Err(InternalError(f"Internal error {err}"))

    def get_resources(self, ids: Optional[list[int]] = None, types: Optional[list[str]] = None) -> Result[list[Resource], Error]:
        return self.__storage.read_resources(ids, types)

    def create_resource(self, req: NewResourceDTO) -> Result[Resource, Error]:
        if req.name.strip() == "":
            return Err(ValidationError("'name' can't be empty"))
        if req.speed <= 0:
            return Err(ValidationError("'speed' must be greater than zero"))
        if req.type.strip() == "":
            return Err(ValidationError("'type' can't be empty"))

        match self.__storage.create_resource(req):
            case Ok(resource_type):
                return Ok(resource_type)
            case Err(DBError('type not found')):
                return Err(ValidationError("'type' unknown"))
            case Err(err):
                return Err(InternalError(f"Internal error {err}"))

    def update_resource(self, req: UpdateResourceDTO) -> Result[Resource, Error]:
        if req.name.strip() == "":
            return Err(ValidationError("'name' can't be empty"))
        if req.speed <= 0:
            return Err(ValidationError("'speed' must be greater than zero"))
        if req.type.strip() == "":
            return Err(ValidationError("'type' can't be empty"))

        match self.__storage.update_resource(req):
            case Ok(resource):
                return Ok(resource)
            case Err(DBNotFound()):
                return Err(NotFound())
            case Err(DBError('type not found')):
                return Err(ValidationError("'type' unknown"))
            case Err(err):
                return Err(InternalError(f"Internal error {err}"))

    def delete_resources(self, ids: list[int]) -> Result[list[ResourceType], Error]:
        match self.__storage.delete_resources(ids):
            case Ok(removed):
                return Ok(removed)
            case Err(err):
                return Err(InternalError(f"Internal error {err}"))
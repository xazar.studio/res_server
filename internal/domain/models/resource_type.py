from dataclasses import dataclass

@dataclass
class NewResourceTypeDTO:
    name: str
    max_speed: int

@dataclass
class UpdateResourceTypeDTO:
    id: int
    name: str
    max_speed: int

@dataclass
class ResourceType:
    id: int
    name: str
    max_speed: int


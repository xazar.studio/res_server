from dataclasses import dataclass
from typing import Optional
from internal.domain.models.resource_type import ResourceType

@dataclass
class NewResourceDTO:
    name: str
    speed: int
    type: str

@dataclass
class UpdateResourceDTO:
    id: int
    name: str
    speed: int
    type: str


@dataclass
class Resource:
    id: int
    name: str
    type: ResourceType
    speed: int

    @property
    def over_speed(self) -> Optional[float]:
        if self.speed > self.type.max_speed:
            return round((self.speed / self.type.max_speed - 1) * 100)
        else:
            return None


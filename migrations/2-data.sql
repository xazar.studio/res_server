INSERT INTO resource_types(name, max_speed) VALUES
    ('car', 90),
    ('tractor', 50),
    ('donkey', 15);

INSERT INTO resources (resource_type, name, cur_speed) VALUES
    (1, 'Wagen', 130),
    (2, 'Cat', 10),
    (3, 'Donkey', 110),
    (1, 'Bugatti', 100);
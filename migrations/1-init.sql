CREATE TABLE resource_types (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    max_speed INTEGER NOT NULL,
    UNIQUE(name)
);

CREATE TABLE resources (
    id SERIAL PRIMARY KEY,
    resource_type INTEGER NOT NULL REFERENCES resource_types(id),
    name VARCHAR(255) NOT NULL,
    cur_speed INTEGER NOT NULL,
    UNIQUE(name)
);